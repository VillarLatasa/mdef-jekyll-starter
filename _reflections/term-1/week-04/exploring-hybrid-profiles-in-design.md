---
title: 04. Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/W4_Hybrid.jpg)
![]({{site.baseurl}}/W4_Hybrid2.jpg)
![]({{site.baseurl}}/W4_Hybrid3.jpg)
![]({{site.baseurl}}/W4_Hybrid4.jpg)
![]({{site.baseurl}}/W4_Hybrid5.jpg)
![]({{site.baseurl}}/W4_Hybrid6.jpg)
![]({{site.baseurl}}/W4_Hybrid7.jpg)
![]({{site.baseurl}}/W4_Hybrid8.jpg)
![]({{site.baseurl}}/W4_Hybrid9.jpg)
![]({{site.baseurl}}/W4_Hybrid10.jpg)
